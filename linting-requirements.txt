black==22.12.0
doc8==1.1.1
djlint==1.19.16
flake8==6.0.0
isort==5.11.4
mypy==0.991
pydocstyle==6.1.1
