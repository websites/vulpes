### Attacker
Útočící aplikace napodobující eshop, snaží se donutit uživatele k přihlášení.
Zpracovává requesty XSS skriptů, rozesílá phishing maily a zprávy.
Umí zobrazit ukradená data.
