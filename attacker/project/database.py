"""Database module, SQLAlchemy object and models."""

import os

from flask import Flask
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()


class MailUser(db.Model):  # type: ignore[name-defined]
    """Mail user model."""

    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(64))
    password = db.Column(db.String(128))


class ShopCookie(db.Model):  # type: ignore[name-defined]
    """Shop cookie model."""

    id = db.Column(db.Integer, primary_key=True)
    cookie = db.Column(db.String(256))


def init_db(app: Flask) -> None:
    """Database initialization."""

    app.config["SQLALCHEMY_DATABASE_URI"] = os.environ.get("DATABASE_URL", "sqlite:///attacker.db")

    db.init_app(app)
    with app.app_context():
        db.create_all()


def add_cookie(cookie: str) -> None:
    """Add a cookie to the database."""

    cookie = cookie.lstrip("session=")
    shop_cookie = ShopCookie(cookie=cookie)
    db.session.add(shop_cookie)
    db.session.commit()
