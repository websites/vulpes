"""Flask application module."""

import os

from flask import Flask
from flask_cors import CORS

from .database import init_db
from .routes.index import bp as index_bp
from .routes.mail import bp as mail_bp
from .routes.shop import bp as shop_bp


def create_app() -> Flask:
    """Create an application instance."""

    app = Flask(__name__)
    app.config["SECRET_KEY"] = os.urandom(32)

    init_db(app)

    app.register_blueprint(index_bp)
    app.register_blueprint(mail_bp)
    app.register_blueprint(shop_bp)

    CORS(app)
    return app
