"""Index route."""

from flask import Blueprint, render_template

bp = Blueprint("index", __name__)


@bp.route("/")
def index() -> str:
    """Index page."""
    return render_template("index.html")
