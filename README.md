# Demonstrační aplikace webových zranitelností

VULnerability PrEsentation Server

Bakalářská práce, Vojtěch Žák

---
# Implementace

Uživatel bude v roli člověka používajícího zranitelné aplikace, následky útoků bude pociťovat ve svých účtech v aplikacích a na stránce zobrazující to, co útočník zjistil.

---
## Email server / klient
Aplikace nahrazující email, umožňující vytvoření účtu, zasílání zpráv ostatním účtům.
Bude existovat pouze jeden server, implementován muže být jednoduchým ukládáním zpráv v databázi.
Nebude přímo demonstrovat žádné zranitelnosti, útočník ho bude využívat k zasílání phishing mailů a k homograph attacku.
Útočník přes ní může posílat maily s odkazy na svojí aplikaci zkoušející CSRF.

## Sociální síť
Aplikace sociální sítě, umožňující přihlášení, vytvoření profilu asociovaného s mailem, komunikaci uživatelů s jinými uživateli.
Zprávy si bude ukládat nezašifrované, bude na ní možné demonstrovat SQL injection na zobrazení zpráv jiných uživatelů.
Útočník přes ní také může zkoušet posílat linky na svojí aplikaci provádějící CSRF.

## Eshop
Eshop umožňující přihlášení, vytvoření účtu asociovaného s mailem, online nakupování.
Uživatel si k účtu může uložit osobní údaje pro urychlení objednávání.
Aplikace je náchylná na XSS, stored scripty může útočník uložit do reviews k produktům.

## Attacker
Útočící aplikace napodobující eshop, snaží se donutit uživatele k přihlášení.
Zpracovává requesty XSS skriptů, rozesílá phishing maily a zprávy.
Umí zobrazit ukradená data.
