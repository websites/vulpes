## Zranitelnosti
Seznam zranitelností demonstrovaných projektem

---
### SQL injection
[wiki](https://en.wikipedia.org/wiki/SQL_injection),
[owasp](https://owasp.org/www-community/attacks/SQL_Injection)

Použití neošetřeného vstupu od útočníka přímo v SQL příkazu.
Pokud si aplikace v databázi ukládá citlivá data,
útočník se k nim může dostat a zneužít například plain-text hesel v jiných aplikacích.

---
### Cross Site Scripting
[wiki](https://en.wikipedia.org/wiki/Cross-site_scripting),
[owasp](https://owasp.org/www-community/attacks/xss/),
[types](https://owasp.org/www-community/Types_of_Cross-Site_Scripting)

Reflected XSS - Zpracování neošetřeného vstupu a zobrazení výstupu  
Stored XSS - Uložení neošetřeného vstupu od útočníka a jeho následná distribuce ostatním uživatelům.  

Reflected formu lze demonstrovat donucením uživatele kliknout na link,
jehož parametr obsahuje script, který je neošetřený použit při načítání stránky
Stored formu lze demonstrovat například knihou návštěv,
do které mohou uživatelé zapsat svoje jméno.
Při prohlížení knihy jiným uživatelem se u něj může spustit útočníkův kód

---
### Cross Site Request Forgery
[wiki](https://en.wikipedia.org/wiki/Cross-site_request_forgery),
[owasp](https://owasp.org/www-community/attacks/csrf)

Nalákání uživatele k navštívení odkazu a provedení nechtěné akce v aplikaci, ve které je přihlášen.
Dá se provést například 0x0 obrázkem načteným z útočníkovy url,
pokud zranitelná aplikace používá jen GET requesty.

---
### Homograph attack
[wiki](https://en.wikipedia.org/wiki/IDN_homograph_attack),
[owasp](https://owasp.org/www-pdf-archive/Unicode_Transformations_Finding_Elusive_Vulnerabilities-Chris_Weber.pdf)

Zneužití podobně vypadajících znaků k phishingu, atd.  
Například &#79;, &#927;, &#1054; - latinka, alfabeta, azbuka - spousta dalších  

Lze demonstrovat pomocí zranitelné aplikace, do které se uživatel přihlášuje.
Útočníkova aplikace toho zneužije fake přihlašovacím oknem

---
### Session Hijacking
[wiki](https://en.wikipedia.org/wiki/Session_hijacking),
[owasp](https://owasp.org/www-community/attacks/Session_hijacking_attack)

Získání session cookie používané uživatelem a její zneužití.
Dá se provést například přes XSS, kterým si útočník opatří session cookie.
S ní se poté může vydávat za uživatele a provádět akce bez jeho vědomí.

---
### Business logic
[owasp](https://owasp.org/www-community/vulnerabilities/Business_logic_vulnerability)

Chyby ve fungování aplikace, které mohou vyústit ve snížené zisky, atd.
Lze demonstovat například aplikací pro nákup vstupenek do kina, která umožňuje rezervaci s pozdější platbou.


## Další poznámky

---
### Michal Zalewski
[Browser Security Handbook](https://code.google.com/archive/p/browsersec/wikis/Part1.wiki#Hypertext_Markup_Language)  
[new blog](https://lcamtuf.coredump.cx/)  
[old blog](https://lcamtuf.blogspot.com/)  
