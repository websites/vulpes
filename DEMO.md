# Demonstration

Má být uživatel v pozici útočníka / oběti?
Nebo nejdříve oběti a potom si zkusit zranitelnosti sám?

## Steps

### Mail
1. Vytvoření mail účtu
2. Obdržení welcome message
3. Obdržení phishing mailu např. o vylepšení účtu
4. Přihlášení se na fake mail loginu
5. Poučení o phishingu a o Unicode znacích

### Chat
1. Vytvoření účtu
2. Potvrzení v mailu
3. Přihlášení
4. Welcome message od admina
5. Zkusit znovu phishing?
6. Prozradit SQL injection, zobrazit jeho zprávy
7. Poučení o SQL injection
8. Obdržení odkazu na útočníkovu stránku
9. Poklikání tlačítek
10. Zjistit, že jsme odeslali nějaké zprávy
11. Poučení o CSRF

### Shop
1. Rozkliknutí konkrétního produktu
2. Vyskočí alert s cookie
3. Objednání produktu, v košíku jich bude víc
4. Poučení o XSS
5. Vytvoření účtu, přihlášení, zadání doručovacích údajů
6. Ukázat uživateli jeho údaje
7. Poučení o cookies

### SQL injection 1
1. přihlášení do chatu
2. zkusit si vyhledat zprávy jiného uživatele

### Homograph attack
1. vytvořit si chat účet `admin` + číslice
2. přihlásit se na admin účet
3. obdržení informace o databázi, názvech tabulek, sloupců

### SQL injection 2
1. vyhledat seznam uživatelů s pomocí UNION SELECT
2. vyhledat seznam uživatelů s hesly

### XSS
1. napsat si script na objednání produktu
2. vložit ho do review produktu






