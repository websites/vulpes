
var language = window.localStorage.getItem('language') || 'cs';
var fakeLoginUrl;

$(document).ready(function(){
    
    changeLanguage(language);
    $(".toggle button").click(function(){
        var id = $(this).attr('id');
        changeLanguage(id);
    });

    $('#email').keypress(e => { if (e.which == 13) sendEmail(); });
    $('.send').click(sendEmail);
});

function changeLanguage(id) {

    $(".toggle button").removeClass("active");
    $(".toggle #" + id).addClass("active");

    $(`.lang`).hide();
    $(`.lang.${id}`).show();

    window.localStorage.setItem('language', id);
}

function sendEmail() {
    $.ajax({
        url: $('#url').val(),
        type: 'POST',
        data: { "email": $('#email').val() },
        success: function(data) {
            $('#email').val('');
            $('#email').css('background-color', 'lightgreen');
        },
        error: function(data) {
            $('#email').val('');
            $('#email').css('background-color', 'lightred');
        }
    });
}
