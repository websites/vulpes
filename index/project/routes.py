"""Routes module."""

from os import environ
from typing import Union

from flask import Blueprint, redirect, render_template, url_for
from werkzeug import Response

from .data import apps, tasks, vulns

bp = Blueprint("routes", __name__)


@bp.route("/")
def index() -> str:
    """Index page."""
    return render_template("index.html", apps=apps, vulns=vulns, tasks=tasks, environ=environ)


@bp.route("/404")
def not_found() -> str:
    """404 page."""
    return render_template("404.html")


@bp.route("/vuln/<id>")
def vuln(id: str) -> Union[Response, str]:
    """Vulnerability page."""
    if id not in vulns:
        return redirect(url_for("routes.index"))
    return render_template(f"vulns/{id}.html")


@bp.route("/task/<id>/")
def task(id: str) -> Response:
    """Render the page."""
    if id not in tasks:
        return redirect(url_for("routes.index"))
    return redirect(url_for("routes.step", id=id, i=1))


@bp.route("/task/<id>/<int:i>/")
def step(id: str, i: int) -> Union[Response, str]:
    """Render the task step page."""

    step_count = int(tasks[id]["steps"])  # type: ignore[call-overload]

    if id in tasks and 1 <= i <= step_count:
        return render_template(f"tasks/{id}/{i}.html", id=id, i=i, len=tasks[id]["steps"], environ=environ)

    return redirect(url_for("routes.index"))


@bp.route("/fake-login/")
def fake_login() -> str:
    """Get the URL of the fake login page."""
    return environ["ATTACKER_URL"] + "/upgrade/"
