"""Flask application module."""

import os

from flask import Flask

from .routes import bp


def create_app() -> Flask:
    """Create a Flask application."""

    app = Flask(__name__)
    app.config["SECRET_KEY"] = os.urandom(32)

    app.register_blueprint(bp)

    return app
