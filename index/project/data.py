"""Data module."""

apps = {
    "mail": {
        "name": "Vulpes Mail",
        "desc": {
            "cs": "Jednoduchá mailová aplikace, která není zranitelná žádným známým typem útoku.",
            "en": "Simple webmail application that is not vulnerable to any known type of attack.",
        },
        "link": "MAIL_URL",
    },
    "chat": {
        "name": "Vulpes Chat",
        "desc": {
            "cs": "Jednoduchá chatovací aplikace, její vyhledávací funkce je zranitelná na SQL injection.",
            "en": "Simple chat application, its search feature is vulnerable to SQL injection.",
        },
        "link": "CHAT_URL",
    },
    "shop": {
        "name": "Vulpes Shop",
        "desc": {
            "cs": "Jednoduchá e-shop aplikace, její funkce recenzí je zranitelná na Cross-site scripting.",
            "en": "Simple webshop application, its review feature is vulnerable to Cross-site scripting.",
        },
        "link": "SHOP_URL",
    },
    "attacker": {
        "name": "Vulpes Attacker",
        "desc": {
            "cs": "Aplikace zneužívající zranitelností v jiných aplikacích, zobrazuje ukradená data.",
            "en": "Application that is used to exploit vulnerabilities in other applications and display stolen data.",
        },
        "link": "ATTACKER_URL",
    },
}

vulns = {
    "sql": {
        "name": "SQL Injection",
        "desc": {
            "cs": "Nekontrolovaný vstup uživatele je vložen do SQL příkazu, který je poté vykonán na databázi.\
                Toho lze zneužít k prohlížení a úpravě dat v databázi.",
            "en": "Unchecked user input is inserted into SQL statements that are then executed on the database.\
                This can be used to view and modify database data.",
        },
    },
    "xss": {
        "name": "Cross-Site Scripting",
        "desc": {
            "cs": "Nekontrolovaný vstup uživatele je vložen do HTML stránky.\
                Toho lze zneužít k vložení škodlivého JavaScript kódu, který bude vykonán v prohlížeči oběti.",
            "en": "Unchecked user input is inserted into an HTML page.\
                This can be used to insert malicious JavaScript code that will be executed in the victim's browser.",
        },
    },
    "csrf": {
        "name": "Cross-Site Request Forgery",
        "desc": {
            "cs": "Útočník bez vědomí uživatele provádí akce v aplikaci, ve které je oběť přihlášena.",
            "en": "The attacker performs actions without users consent in the app in which the victim is logged in.",
        },
    },
    "phish": {
        "name": "Phishing",
        "desc": {
            "cs": "Podvody zneužívající důvěru oběti za účelem získání citlivých informací.",
            "en": "Frauds that exploit the victim's trust in order to obtain sensitive information.",
        },
    },
}

tasks = {
    "phish": {
        "name": "Phishing",
        "desc": {
            "cs": "Podvody zneužívající důvěru oběti za účelem získání citlivých informací.",
            "en": "Frauds that exploit the victim's trust in order to obtain sensitive information.",
        },
        "steps": 7,
    },
    "unicode": {
        "name": "Unicode",
        "desc": {
            "cs": "Jak zneužít existenci Unicode znaků?",
            "en": "How to exploit Unicode characters?",
        },
        "steps": 6,
    },
    "sql1": {
        "name": "SQL OR Injection",
        "desc": {"cs": "Jak dostat cizí data z databáze?", "en": "How to get someone else's data from the database?"},
        "steps": 4,
    },
    "sql2": {
        "name": "SQL UNION SELECT Injection",
        "desc": {
            "cs": "Jak získat data z jiných tabulek databáze?",
            "en": "How to access data from other tables in the database?",
        },
        "steps": 5,
    },
    "xss": {
        "name": "Cross-Site Scripting",
        "desc": {
            "cs": "Jak vložit JavaScript do stránky HTML?",
            "en": "How to insert JavaScript into an HTML page?",
        },
        "steps": 4,
    },
    "hijack": {
        "name": "Session Hijacking",
        "desc": {
            "cs": "Jak získat přístup k cizímu účtu?",
            "en": "How to gain access to someone else's account?",
        },
        "steps": 6,
    },
    "csrf": {
        "name": "Cross-Site Request Forgery",
        "desc": {
            "cs": "Provádění nechtěných akcí jménem jiného uživatele.",
            "en": "Performing unwanted actions on behalf of another user.",
        },
        "steps": 6,
    },
}
