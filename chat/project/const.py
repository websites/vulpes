"""Module for constants."""

MIN_PASSWORD_LENGTH = 8
MAX_PASSWORD_LENGTH = 128

ALICE = "alice@vulpes.nic.cz"
BOB = "bob@vulpes.nic.cz"
