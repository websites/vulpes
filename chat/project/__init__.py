"""Flask application module."""

import os

from flask import Flask

from .database import init_db
from .events import socketio
from .login import init_login
from .routes.admin import bp as admin_bp
from .routes.auth import bp as auth_bp
from .routes.chat import bp as chat_bp


def create_app() -> Flask:
    """Create an application instance."""

    app = Flask(__name__)
    app.config["SECRET_KEY"] = os.urandom(32)

    init_db(app)
    init_login(app)

    app.register_blueprint(chat_bp)
    app.register_blueprint(auth_bp)
    app.register_blueprint(admin_bp)

    socketio.init_app(app)
    return app
