"""Module for handling SocketIO events."""

from flask import request
from flask_login import current_user
from flask_socketio import SocketIO
from sqlalchemy import text

from .database import User, add_message, db

clients = {}
socketio = SocketIO()


@socketio.on("join")
def connect() -> None:
    """Connect a user."""
    print("User", current_user.email, "connected")
    clients[current_user.email] = request.sid  # type: ignore[attr-defined]


@socketio.on("disconnect")
def disconnect() -> None:
    """Disconnect a user."""
    print("User", current_user.email, "disconnected")
    clients.pop(current_user.email, None)


@socketio.on("message")
def message(message: dict) -> None:
    """Send a message to a user."""

    sender = current_user.email
    recipient = message["recipient"]
    text = message["text"]

    add_message(sender, recipient, text)

    if recipient in clients and recipient != sender:
        socketio.emit("message", message, room=clients[recipient])


@socketio.on("search")
def search(data: dict) -> None:
    """Search for a message."""

    partner = User.query.filter_by(email=data["partner"]).first()

    with db.engine.connect() as con:

        query = f"""SELECT * FROM message WHERE
            (sender_id = '{partner.id}' OR recipient_id = '{partner.id}')
            AND (sender_id = '{current_user.id}' OR recipient_id = '{current_user.id}')
            AND message LIKE '%{data['query']}%'
            ORDER BY timestamp;"""

        res = con.execute(text(query)).fetchall()

        messages = []

        for id, message, sender_id, recipient_id, timestamp in res:
            sender = User.query.filter_by(id=sender_id).first()
            recipient = User.query.filter_by(id=recipient_id).first()

            messages.append(
                {
                    "text": message,
                    "sender": {"email": sender.email, "username": sender.username},
                    "recipient": recipient.email,
                }
            )

    socketio.emit("search", messages, room=clients[current_user.email])
