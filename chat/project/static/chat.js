var socket = io();

var current_user = {}
var partner = {}

socket.on('connect', () => socket.emit('join'));
socket.on('message', message => {
	
	if (message.sender.email == partner.email)
		renderMessage(message, $('#messages'))
	updateContact(message);
});
socket.on('search', messages => {
	$('#found').empty();
	messages.forEach(m => renderMessage(m, $('#found')));
});

$(document).ready(() => {

	$.ajax({
		url: $(location).attr('href') + '/data',
		method: 'GET',
		success: data => {
			current_user = data['current_user'];
			partner = data['partner'];
		},
		error: err => console.log(err)
	});

	$('#message').keypress(e => { if (e.which == 13) sendMessage(); });
	$('#messages').scrollTop($('#messages')[0].scrollHeight);
	$('#search').keypress(e => { if (e.which == 13) search(); });
});

function sendMessage() {

	if ($('#message').val() == '') return;

	message = {
		'text': $('#message').val(),
		'sender': {
			'email': current_user.email,
			'username': current_user.username,
		},
		'recipient': partner.email
	}
	socket.emit('message', message);
	renderMessage(message, $('#messages'));
	updateContact(message);
	$('#message').val('');
}

function search() {
	query = {
		'query': $('#search').val(),
		'partner': partner.email
	}
	socket.emit('search', query);
}

function renderMessage(message, element) {

	const cls = message.sender.email == current_user.email ? 'sent' : 'received';
	element.append(`<li class="message ${cls}"><div> ${message.sender.username}: ${message.text} </div></li>`);
	element.scrollTop(element[0].scrollHeight);
}

function updateContact(message) {

	var message_div;

	if (message.sender.email == current_user.email)
		message_div = $(`#contacts a[href$="${(message.recipient)}"] div.last_message`);

	else
		message_div = $(`#contacts a[href$="${(message.sender.email)}"] div.last_message`);  


	if (message_div.length > 0) {
		message_div.text(message.text);
	}
	
	else {
		$('#contacts').append(
			`<a href="/chat/${message.sender}">
				<li class="contact">
					<div class="name">${message.sender}</div>
					<div class="last_message">${message.text}</div>
					<div class="timestamp">${message.timestamp}</div>
				</li>
			</a>`
		);
	}
}