"""Authentication routes module."""

from flask import Blueprint, flash, redirect, render_template, request, url_for
from flask_login import login_required, login_user, logout_user
from werkzeug.wrappers import Response

from ..database import User, db, send_welcome_message
from ..forms import LoginForm, SignupForm

bp = Blueprint("auth", __name__)


@bp.route("/login/")
def login() -> str:
    """Render the login page."""
    return render_template("auth/login.html", form=LoginForm())


@bp.route("/login/", methods=["POST"])
def login_post() -> Response:
    """Handle the login request."""

    form = LoginForm(request.form)
    email = form.email.data
    password = form.password.data

    if "@" in email:
        user = User.query.filter_by(email=email).first()
    else:
        user = User.query.filter_by(username=email).first()

    if not user or user.password != password:
        flash("Invalid username/password combination")
        return redirect(url_for("auth.login"))

    else:
        login_user(user)
        return redirect(url_for("chat.chat"))


@bp.route("/signup/")
def signup() -> str:
    """Render the signup page."""
    return render_template("auth/signup.html", form=SignupForm())


@bp.route("/signup/", methods=["POST"])
def signup_post() -> Response:
    """Handle the signup request."""

    form = SignupForm(request.form)
    email = form.email.data
    username = form.username.data
    password = form.password.data

    if User.query.filter_by(email=email).first():
        flash("Email address already exists")
        return redirect(url_for("auth.signup"))

    if User.query.filter_by(username=username).first():
        flash("Username already exists")
        return redirect(url_for("auth.signup"))

    user = User(email=email, password=password, username=username)
    db.session.add(user)
    db.session.commit()

    send_welcome_message(user)

    login_user(user)
    return redirect(url_for("chat.chat"))


@bp.route("/logout/", methods=["POST"])
@login_required
def logout() -> Response:
    """Logout the current user."""

    logout_user()
    flash("You have been logged out.", "success")
    return redirect(url_for("auth.login"))
