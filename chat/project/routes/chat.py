"""Module handling the main application routes."""

from flask import Blueprint, flash, redirect, render_template, request, url_for
from flask_login import current_user, login_required
from werkzeug.wrappers import Response

from ..database import Contact, Message, User, db
from ..forms import ContactForm, SendForm

bp = Blueprint("chat", __name__)


@bp.route("/")
def index() -> Response:
    """Redirect logged users to the chat, others to the login page."""

    if current_user:
        return redirect(url_for("chat.chat", partner=""))
    else:
        return redirect(url_for("auth.login"))


@bp.route("/chat/")
@bp.route("/chat/<email>")
@login_required
def chat(email: str = "") -> str:
    """Render the chat page.

    Chat page can have a conversation
        - open - email is the email of the partner,
        - closed - email is empty,
    """

    contacts = Contact.query.filter_by(user_id=current_user.id).all()

    if not email:
        return render_template("chat/index.html", form=SendForm(), contacts=contacts, partner=None)

    partner = User.query.filter_by(email=email).first()
    if not partner:
        return render_template("chat/index.html", form=SendForm(), contacts=contacts, partner=None)

    sent = Message.query.filter_by(sender_id=current_user.id, recipient_id=partner.id)
    received = Message.query.filter_by(recipient_id=current_user.id, sender_id=partner.id)
    messages = sent.union(received).order_by(Message.timestamp).all()

    return render_template("chat/index.html", form=SendForm(), contacts=contacts, partner=partner, messages=messages)


@bp.route("/chat/<email>/data")
@login_required
def chat_data(email: str) -> object:
    """Return data about the current user and the current partner."""

    partner = User.query.filter_by(email=email).first()
    if not partner:
        return "Error: partner does not exist"

    return {
        "current_user": {
            "email": current_user.email,
            "username": current_user.username,
        },
        "partner": {"email": email, "username": partner.username},
    }


@bp.route("/contacts/", methods=["POST"])
@login_required
def contacts() -> Response:
    """Add a new contact to the database."""

    form = ContactForm(request.form)
    email = form.email.data

    partner = User.query.filter_by(email=email).first()

    if not partner:
        flash("User does not exist")
        return redirect(url_for("chat.chat"))

    contact = Contact.query.filter_by(user_id=current_user.id, partner_id=partner.id).first()

    if not contact:
        contact = Contact(user_id=current_user.id, partner_id=partner.id, last_message_id=None)
        db.session.add(contact)
        db.session.commit()

    return redirect(url_for("chat.chat", email=email))
