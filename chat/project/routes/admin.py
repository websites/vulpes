"""Module handling the admin routes."""

from typing import Union

from flask import Blueprint, redirect, render_template, url_for
from flask_login import current_user, login_required
from werkzeug.wrappers import Response

from ..database import is_admin

bp = Blueprint("admin", __name__)


@bp.route("/admin/")
@login_required
def admin() -> Union[Response, str]:
    """Render the admin page."""

    if is_admin(current_user):
        return render_template("admin/admin.html")

    return redirect(url_for("chat.chat"))
