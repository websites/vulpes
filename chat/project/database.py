"""Database module containing the database object and models."""

import os
from datetime import datetime

from flask import Flask
from flask_login import UserMixin
from flask_sqlalchemy import SQLAlchemy

from .const import ALICE, BOB, MAX_PASSWORD_LENGTH

db = SQLAlchemy()


class User(db.Model, UserMixin):  # type: ignore[name-defined]
    """User model."""

    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(64), unique=True)
    username = db.Column(db.String(64), unique=True)
    password = db.Column(db.String(MAX_PASSWORD_LENGTH))


class Message(db.Model):  # type: ignore[name-defined]
    """Chat message model."""

    id = db.Column(db.Integer, primary_key=True)
    message = db.Column(db.String(4096))
    sender_id = db.Column(db.Integer, db.ForeignKey("user.id"))
    recipient_id = db.Column(db.Integer, db.ForeignKey("user.id"))
    timestamp = db.Column(db.DateTime, server_default=db.func.now())

    sender = db.relationship("User", foreign_keys=[sender_id])
    recipient = db.relationship("User", foreign_keys=[recipient_id])


class Contact(db.Model):  # type: ignore[name-defined]
    """Contact model.

    This model is used to store the contact of a user along with the last message sent.
    Only points in one direction, from user to partner, not the other way around.
    """

    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey("user.id"))
    partner_id = db.Column(db.Integer, db.ForeignKey("user.id"))
    last_message_id = db.Column(db.Integer, db.ForeignKey("message.id"), nullable=True)

    user = db.relationship("User", foreign_keys=[user_id])
    partner = db.relationship("User", foreign_keys=[partner_id])
    last_message = db.relationship("Message", foreign_keys=[last_message_id])


def init_db(app: Flask) -> None:
    """Initialize the database."""

    app.config["SQLALCHEMY_DATABASE_URI"] = os.environ.get("DATABASE_URL", "sqlite:///chat.db")

    db.init_app(app)
    with app.app_context():
        db.create_all()
        fill_db()


def is_admin(user: User) -> bool:
    """Check if the user is an admin."""

    name = user.username

    if len(name) != 6 or not name.startswith("admin"):
        return False

    return name[-1].isdigit()


def add_message(sender_email: str, recipient_email: str, text: str) -> None:
    """Add a message to the database."""

    sender = User.query.filter_by(email=sender_email).first()
    recipient = User.query.filter_by(email=recipient_email).first()
    message = Message(message=text, sender=sender, recipient=recipient, timestamp=datetime.now())

    db.session.add(message)
    db.session.commit()

    contact = Contact.query.filter_by(user_id=sender.id, partner_id=recipient.id).first()
    if not contact:
        contact = Contact(user=sender, partner=recipient)
        db.session.add(contact)
    contact.last_message = message

    contact = Contact.query.filter_by(user_id=recipient.id, partner_id=sender.id).first()
    if not contact:
        contact = Contact(user=recipient, partner=sender)
        db.session.add(contact)

    contact.last_message = message

    db.session.commit()


def send_welcome_message(user: User) -> None:
    """Send a welcome message to a new user."""

    admin_no = user.id % 10  # pick a random admin

    if is_admin(user):
        message = f"Welcome to Vulpes chat, {user.username}! You are an admin. Admin page is at /admin."
    else:
        message = f"Welcome to Vulpes chat, {user.username}!"

    add_message(f"admin{admin_no}@vulpes.nic.cz", user.email, message)


def add_admins() -> None:
    """Add admins to the database."""

    for i in range(10):
        name = f"admin{i}"
        email = f"admin{i}@vulpes.nic.cz"

        admin = User.query.filter_by(email=email).first()
        if not admin:
            admin = User(email=email, password=name, username=name)
            db.session.add(admin)

    db.session.commit()


def add_conversation() -> None:
    """Add a conversation between Alice and Bob."""

    alice = User.query.filter_by(email=ALICE).first()
    bob = User.query.filter_by(email=BOB).first()

    if alice and bob:
        return

    alice = User(email=ALICE, username="alice_liddell", password="alice123liddell")
    bob = User(email=BOB, username="bob_hatter", password="bob123hatter")

    db.session.add(alice)
    db.session.add(bob)
    db.session.commit()

    add_message(ALICE, BOB, "Hey Bob! Long time no talk! How's it going?")
    add_message(BOB, ALICE, "Hey Alice! I'm good! How's Karen doing?")
    add_message(ALICE, BOB, "She's doing great! Started kindergarten this year.")
    add_message(BOB, ALICE, "Wow, time flies! I remember when she was just a baby.")
    add_message(ALICE, BOB, "I know, right? How's your tea shop doing?")
    add_message(BOB, ALICE, "It's going great! I'm thinking of expanding to a second location.")
    add_message(ALICE, BOB, "That's great! I'll have to come visit sometime.")
    add_message(BOB, ALICE, "That would be great! I'll let you know when I open.")
    add_message(ALICE, BOB, "Sounds good! See you later!")
    add_message(BOB, ALICE, "Cya")


def fill_db() -> None:
    """Fill the database with some Alice and Bob messages."""

    add_admins()
    add_conversation()
