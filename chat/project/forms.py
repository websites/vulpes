"""Forms for the chat app."""

from flask_wtf import FlaskForm
from wtforms.fields import EmailField, PasswordField, StringField, SubmitField
from wtforms.validators import EqualTo, InputRequired, Length

from .const import MAX_PASSWORD_LENGTH, MIN_PASSWORD_LENGTH


class SignupForm(FlaskForm):
    """Sign up form."""

    username = StringField("username", validators=[InputRequired(), Length(6, 64)])
    email = EmailField("email", validators=[InputRequired(), Length(1, 64)])
    password = PasswordField("password", validators=[InputRequired(), Length(MIN_PASSWORD_LENGTH, MAX_PASSWORD_LENGTH)])
    password2 = PasswordField("repeat password", validators=[EqualTo("pwd", message="Passwords must match!")])
    submit = SubmitField("Sign Up")


class LoginForm(FlaskForm):
    """Log in form."""

    email = StringField("username or email", validators=[InputRequired(), Length(1, 64)])
    password = PasswordField("password", validators=[InputRequired(), Length(MIN_PASSWORD_LENGTH, MAX_PASSWORD_LENGTH)])
    submit = SubmitField("Log In")


class SendForm(FlaskForm):
    """Form for sending a message."""

    message = StringField("message", validators=[InputRequired(), Length(1, 4096)])
    submit = SubmitField("Send")


class ContactForm(FlaskForm):
    """Form for adding a new contact."""

    email = EmailField("email", validators=[InputRequired(), Length(1, 64)])
    submit = SubmitField("Add Contact")
