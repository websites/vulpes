"""Module handling the authentication of users."""

from flask import Flask
from flask_login import LoginManager

from .database import User

login_manager = LoginManager()


@login_manager.user_loader
def load_user(user_id: int) -> User:
    """Get the user by id."""
    return User.query.get(int(user_id))


def init_login(app: Flask) -> None:
    """Initialize the login manager."""

    login_manager.login_view = "auth.login"
    login_manager.init_app(app)
