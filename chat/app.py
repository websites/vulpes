#!/usr/bin/env python3
"""Chat app."""

from project import create_app, socketio

app = create_app()

if __name__ == "__main__":
    socketio.run(app)
