### Sociální síť
Aplikace sociální sítě, umožňující přihlášení, vytvoření profilu asociovaného s mailem, komunikaci uživatelů s jinými uživateli.
Zprávy si bude ukládat nezašifrované, bude na ní možné demonstrovat SQL injection na zobrazení zpráv jiných uživatelů.
Útočník přes ní také může zkoušet posílat linky na svojí aplikaci provádějící CSRF.
