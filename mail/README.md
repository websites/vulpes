### Email server / klient
Aplikace nahrazující email, umožňující vytvoření účtu, zasílání zpráv ostatním účtům.
Bude existovat pouze jeden server, implementován muže být jednoduchým ukládáním zpráv v databázi.
Nebude přímo demonstrovat žádné zranitelnosti, útočník ho bude využívat k zasílání phishing mailů a k homograph attacku.
Útočník přes ní může posílat maily s odkazy na svojí aplikaci zkoušející CSRF.
