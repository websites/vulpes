"""Module handling the routes inside the application."""

import re
from datetime import datetime
from typing import Union

from flask import Blueprint, flash, redirect, render_template, request, url_for
from flask_cors import cross_origin
from flask_login import current_user, login_required
from werkzeug.wrappers import Response

from ..database import Message, db
from ..forms import ComposeForm

bp = Blueprint("mail", __name__)


@bp.route("/")
def home() -> Response:
    """Redirect logged users to inbox, others to login."""

    if current_user:
        return redirect(url_for("mail.inbox"))
    else:
        return redirect(url_for("auth.login"))


@bp.route("/inbox/")
@login_required
def inbox() -> str:
    """Display the inbox of the current user."""

    messages = Message.query.filter_by(recipient=current_user.email).order_by(Message.timestamp).all()
    return render_template("mail/inbox.html", user=current_user, messages=messages)


@bp.route("/compose/")
@login_required
def compose() -> str:
    """Render the compose page."""
    return render_template("mail/compose.html", form=ComposeForm())


@bp.route("/compose/", methods=["POST"])
@login_required
@cross_origin()
def compose_post() -> Response:
    """Handle the sent message."""

    recipient = request.form.get("recipient")
    subject = request.form.get("subject")
    body = request.form.get("body")

    message = Message(
        sender=current_user.email,
        recipient=recipient,
        subject=subject,
        body=body,
        timestamp=datetime.now(),
    )
    db.session.add(message)
    db.session.commit()

    flash("Message sent!")
    return redirect(url_for("mail.inbox"))


@bp.route("/sent/")
@login_required
def sent() -> str:
    """Render the sent messages."""

    messages = Message.query.filter_by(sender=current_user.email).order_by(Message.timestamp).all()
    return render_template("mail/sent.html", user=current_user, messages=messages)


@bp.route("/message/<id>")
@login_required
def message(id: int) -> Union[Response, str]:
    """Render the message with the given id."""

    message = Message.query.filter_by(id=id).first()
    if message.recipient != current_user.email and message.sender != current_user.email:
        return redirect(url_for("mail.inbox"))

    return clickify_links(render_template("mail/message.html", user=current_user, message=message))


def clickify_links(text: str) -> str:
    """Replace all links in the text with clickable links."""

    regex = re.compile("(https?:\\/\\/(www\\.)?(localhost\\S*|\\S+\\.\\S+))")
    return regex.sub(r"<a href='\1'>\1</a>", text)
