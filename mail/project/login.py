"""Module for login management."""

from flask import Flask
from flask_login import LoginManager

from .database import User

login_manager = LoginManager()


@login_manager.user_loader
def load_user(user_id: int) -> User:
    """Load a user by ID."""
    return User.query.get(user_id)


def init_login(app: Flask) -> None:
    """Initialize login manager."""
    login_manager.login_view = "auth.login"
    login_manager.init_app(app)
