"""Flask application module."""

import os

from flask import Flask
from flask_cors import CORS

from .database import init_db
from .login import init_login
from .routes.auth import bp as auth_bp
from .routes.mail import bp as mail_bp


def create_app() -> Flask:
    """Create an application instance."""

    app = Flask(__name__)
    app.config["SECRET_KEY"] = os.urandom(32)

    init_db(app)
    init_login(app)

    app.register_blueprint(auth_bp)
    app.register_blueprint(mail_bp)

    CORS(app)
    return app
