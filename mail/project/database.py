"""Database module, including the SQLAlchemy database object and models."""

import os

from flask import Flask
from flask_login import UserMixin
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()


class User(UserMixin, db.Model):  # type: ignore[name-defined]
    """User account model."""

    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(64), unique=True)
    password = db.Column(db.String(256))


class Message(db.Model):  # type: ignore[name-defined]
    """Message model."""

    id = db.Column(db.Integer, primary_key=True)
    sender = db.Column(db.String(64))
    recipient = db.Column(db.String(64))
    subject = db.Column(db.String(256))
    body = db.Column(db.String(4096))
    timestamp = db.Column(db.DateTime)


def init_db(app: Flask) -> None:
    """Database initialization."""

    app.config["SQLALCHEMY_DATABASE_URI"] = os.environ.get("DATABASE_URL", "sqlite:///mail.db")

    db.init_app(app)
    with app.app_context():
        db.create_all()
