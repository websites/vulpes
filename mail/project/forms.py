"""Module containing forms used in the application."""

from flask_wtf import FlaskForm
from wtforms.fields import PasswordField, StringField, SubmitField, TextAreaField
from wtforms.validators import EqualTo, InputRequired, Length

from .const import PASSWORD_MIN_LENGTH, USERNAME_MAX_LENGTH, USERNAME_MIN_LENGTH


class SignupForm(FlaskForm):
    """Sign up form."""

    username = StringField("username", validators=[InputRequired(), Length(USERNAME_MIN_LENGTH, USERNAME_MAX_LENGTH)])
    password = PasswordField("password", validators=[InputRequired(), Length(PASSWORD_MIN_LENGTH)])
    password2 = PasswordField("repeat password", validators=[EqualTo("password", message="Passwords must match")])
    submit = SubmitField("Sign up")


class LoginForm(FlaskForm):
    """Log in form."""

    email = StringField("username or email", validators=[InputRequired(), Length(USERNAME_MIN_LENGTH)])
    password = PasswordField("password", validators=[InputRequired(), Length(PASSWORD_MIN_LENGTH)])
    submit = SubmitField("Log in")


class ComposeForm(FlaskForm):
    """Form used to compose mails."""

    recipient = StringField("recipient", validators=[InputRequired(), Length(USERNAME_MIN_LENGTH, 64)])
    subject = StringField("subject", validators=[InputRequired(), Length(1, 256)])
    body = TextAreaField("body", validators=[InputRequired(), Length(1, 4096)])
    submit = SubmitField("Send")
