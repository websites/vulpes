Flask==2.3.2
Flask-Cors==3.0.10
Flask-Login==0.6.2
Flask-SQLAlchemy==3.0.3
Flask-WTF==1.1.1
psycopg2-binary==2.9.6
