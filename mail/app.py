#!/usr/bin/env python3
"""Mail app."""

from project import create_app

app = create_app()

if __name__ == "__main__":
    app.run()
