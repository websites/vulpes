"""Flask application module."""

import os

from flask import Flask

from .database import init_db
from .login import init_login
from .routes.auth import bp as auth_bp
from .routes.profile import bp as profile_bp
from .routes.shop import bp as shop_bp


def create_app() -> Flask:
    """Create an application instance."""

    app = Flask(__name__)
    app.config["SECRET_KEY"] = os.urandom(32)
    app.config["SESSION_COOKIE_HTTPONLY"] = False

    init_db(app)
    init_login(app)

    app.register_blueprint(auth_bp)
    app.register_blueprint(shop_bp)
    app.register_blueprint(profile_bp)

    return app
