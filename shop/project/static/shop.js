
function addToCart() {
	
	$.ajax({
		url: '/cart/',
		type: 'POST',
		data: {
			'product_url': window.location.pathname.split('/')[2],
			'quantity': $('#quantity').val()
		},
		success: function(data) {
			$('#quantity').val(1);
			replaceFlashes(data);
		}
	});
}

function removeFromCart(url) {

	$.ajax({
		url: '/cart/',
		type: 'DELETE',
		data: {
			'product_url': url
		},
		success: function(data) {
			$(`#cart-item-${url}`).remove();
			replaceFlashes(data);
			updateTotal();
		}
	});
}

function updateCart(url) {

	quantity = $(`#cart-item-${url} input`).val();
	price = $(`#cart-item-${url} .price`).text();
	total = parseFloat(quantity * price).toFixed(2);

	console.log(`Updating ${url} to ${quantity} at ${price}`);

	$.ajax({
		url: '/cart/',
		type: 'PUT',
		data: {
			'product_url': url,
			'quantity': quantity
		},
		success: function(data) {
			$(`#cart-item-${url} .total`).text(total);
			replaceFlashes(data);
			updateTotal();
		}
	});
}

function updateTotal() {
	
	total = 0;
	$('.total').each((i, e) => total += parseFloat(e.innerText));
	$('#total').text(parseFloat(total).toFixed(2));
}

function replaceFlashes(data) {
	$('.flashes').replaceWith(data);
}

function addReview() {
	
	$.ajax({
		url: window.location.pathname,
		type: 'POST',
		data: {
			'text': $('#review').val()
		},
		success: function(data) {
			$('#review').val('');

			if ($('#reviews #empty').length > 0) {
				$('#reviews #empty').remove();
			}

			$('#reviews').prepend(data);			
		}
	});
}

$(document).ready(() => {
	$('#review').keypress(e => { if (e.which == 13) addReview(); });
	$('.info #quantity').keypress(e => { if (e.which == 13) addToCart(); });
});
