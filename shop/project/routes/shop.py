"""Module handling the shop routes."""

from typing import Dict, Tuple

from flask import Blueprint, flash, redirect, render_template, request, url_for
from flask_cors import cross_origin
from flask_login import current_user, login_required
from werkzeug import Response

from ..database import Order, OrderItem, Product, Review, db
from ..forms import CartForm, DetailsForm, ReviewForm

bp = Blueprint("shop", __name__)


carts: Dict[str, Dict[str, Tuple[Product, int]]] = {}


@bp.route("/")
def index() -> str:
    """Render the shop index page."""
    products = Product.query.all()
    return render_template("shop/index.html", products=products)


@bp.route("/product/<product_url>/")
def product(product_url: str) -> str:
    """Render the product page."""
    product = Product.query.filter_by(url=product_url).first()
    return render_template("shop/product.html", product=product, form=ReviewForm())


@bp.route("/product/<product_url>/", methods=["POST"])
def reviews(product_url: str) -> str:
    """Add a review for a product.

    Returns: The review template.
    """

    form = ReviewForm(request.form)
    product = Product.query.filter_by(url=product_url).first()
    review = Review(text=form.text.data, product_id=product.id)
    db.session.add(review)
    db.session.commit()

    return render_template("elements/review.html", review=review)


@bp.route("/cart/")
@login_required
def cart() -> str:
    """Render the cart page."""

    uid = current_user.email
    products = carts.get(uid, {}).values()
    total = sum(product.price * quantity for product, quantity in products)

    return render_template("shop/cart.html", products=products, total=total)


@bp.route("/cart/", methods=["POST"])
@cross_origin()
@login_required
def cart_post() -> Response:
    """Add a product to the cart."""

    form = CartForm(request.form)
    product_url = form.product_url.data
    quantity = form.quantity.data
    product = Product.query.filter_by(url=product_url).first()

    uid = current_user.email

    if uid not in carts:
        carts[uid] = {}

    current_quantity = carts[uid][product_url][1] if product_url in carts[uid] else 0
    carts[uid][product_url] = (product, current_quantity + quantity)

    flash(f"{product.name} added to cart.")
    return redirect(url_for("shop.cart"))


@bp.route("/cart/", methods=["DELETE"])
@login_required
def cart_delete() -> str:
    """Remove a product from the cart."""

    form = CartForm(request.form)
    product_url = form.product_url.data

    uid = current_user.email
    del carts[uid][product_url]

    flash("Item removed from cart.")
    return render_template("elements/flashes.html")


@bp.route("/cart/", methods=["PUT"])
@login_required
def cart_put() -> str:
    """Update the quantity of a product in the cart."""

    form = CartForm(request.form)
    product_url = form.product_url.data
    quantity = form.quantity.data

    uid = current_user.email
    product, _ = carts[uid][product_url]
    carts[uid][product_url] = (product, quantity)
    flash(f"{product.name} quantity updated.")
    return render_template("elements/flashes.html")


@bp.route("/checkout/")
@login_required
def checkout() -> str:
    """Render the checkout page."""
    return render_template("shop/checkout.html", form=DetailsForm())


@bp.route("/checkout/", methods=["POST"])
@login_required
def checkout_post() -> Response:
    """Checkout the cart."""

    form = DetailsForm(request.form)
    user = current_user
    uid = user.email

    user.first_name = form.first_name.data
    user.last_name = form.last_name.data
    user.address = form.address.data
    user.city = form.city.data
    db.session.commit()

    cart = carts[uid]

    if not cart:
        flash("Your cart is empty.")
        return redirect(url_for("shop.cart"))

    total = sum(product.price * quantity for product, quantity in cart.values())
    order = Order(user_id=user.id, total=total)

    db.session.add(order)
    db.session.commit()

    for product, quantity in cart.values():
        order_item = OrderItem(order_id=order.id, product_id=product.id, quantity=quantity)
        db.session.add(order_item)
        db.session.commit()

    del carts[uid]

    flash("Order placed successfully.")
    return redirect(url_for("shop.index"))
