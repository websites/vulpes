"""Module handling the authentication routes."""

from flask import Blueprint, flash, redirect, render_template, request, url_for
from flask_login import login_required, login_user, logout_user
from werkzeug import Response
from werkzeug.security import check_password_hash, generate_password_hash

from ..database import User, db
from ..forms import LoginForm, SignupForm

bp = Blueprint("auth", __name__)


@bp.route("/signup/")
def signup() -> str:
    """Render the signup page."""
    return render_template("auth/signup.html", form=SignupForm())


@bp.route("/signup/", methods=["POST"])
def signup_post() -> Response:
    """Handle the signup form submission."""

    form = SignupForm(request.form)
    email = form.email.data
    password = form.password.data
    first_name = form.first_name.data
    last_name = form.last_name.data

    if User.query.filter_by(email=email).first():
        flash("An account with this email address already exists, please log in.")
        return redirect(url_for("auth.login"))

    user = User(
        email=email,
        password=generate_password_hash(password),
        first_name=first_name,
        last_name=last_name,
    )
    db.session.add(user)
    db.session.commit()

    login_user(user)
    return redirect(url_for("shop.index"))


@bp.route("/login/")
def login() -> str:
    """Render the login page."""
    return render_template("auth/login.html", form=LoginForm())


@bp.route("/login/", methods=["POST"])
def login_post() -> Response:
    """Handle the login form submission."""

    form = LoginForm(request.form)
    email = form.email.data
    password = form.password.data

    user = User.query.filter_by(email=email).first()

    if not user or not check_password_hash(user.password, password):
        flash("Please check your login details and try again.")
        return redirect(url_for("auth.login"))

    login_user(user)
    return redirect(url_for("shop.index"))


@bp.route("/logout/", methods=["POST"])
@login_required
def logout() -> Response:
    """Logout the current user."""
    logout_user()
    return redirect(url_for("shop.index"))
