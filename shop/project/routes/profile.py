"""Module handling the profile routes."""

from typing import Union

from flask import Blueprint, flash, redirect, render_template, request, url_for
from flask_login import current_user, login_required
from werkzeug import Response

from ..database import Order, User, db
from ..forms import DetailsForm

bp = Blueprint("profile", __name__)


@bp.route("/profile/")
@login_required
def profile() -> str:
    """Render the current user's profile page."""
    return render_template("profile/profile.html", form=DetailsForm())


@bp.route("/profile/", methods=["POST"])
@login_required
def profile_post() -> Response:
    """Update the current user's profile."""

    form = DetailsForm(request.form)

    email = form.email.data
    first_name = form.first_name.data
    last_name = form.last_name.data
    address = form.address.data
    city = form.city.data

    if User.query.filter_by(email=email).first() and email != current_user.email:
        flash("Email address already in use.")
        return redirect(url_for("profile.profile"))

    current_user.email = email
    current_user.first_name = first_name
    current_user.last_name = last_name
    current_user.address = address
    current_user.city = city

    db.session.commit()

    flash("Your profile has been updated.")
    return redirect(url_for("profile.profile"))


@bp.route("/orders/")
@login_required
def orders() -> str:
    """Render page with a list of orders."""

    orders = Order.query.filter_by(user_id=current_user.id).all()
    return render_template("profile/orders.html", orders=orders)


@bp.route("/orders/<int:order_id>/")
@login_required
def order(order_id: int) -> Union[Response, str]:
    """Render the order detail page."""

    order = Order.query.get(order_id)

    if not order or order.user_id != current_user.id:
        flash("Order not found.")
        return redirect(url_for("profile.orders"))

    return render_template("profile/order.html", order=order)
