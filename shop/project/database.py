"""Database module containing the SQLAlchemy database object and models."""

import os

from flask import Flask
from flask_login import UserMixin
from flask_sqlalchemy import SQLAlchemy

from . import const

db = SQLAlchemy()


class User(db.Model, UserMixin):  # type: ignore[name-defined]
    """User account model."""

    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(const.MAX_EMAIL_LENGTH), unique=True)
    password = db.Column(db.String(const.MAX_PASSWORD_LENGTH), server_default="")
    first_name = db.Column(db.String(const.MAX_FIRST_NAME_LENGTH))
    last_name = db.Column(db.String(const.MAX_LAST_NAME_LENGTH))
    address = db.Column(db.String(const.MAX_ADDRESS_LENGTH), server_default="")
    city = db.Column(db.String(const.MAX_CITY_LENGTH), server_default="")


class Product(db.Model):  # type: ignore[name-defined]
    """Product model."""

    id = db.Column(db.Integer, primary_key=True)
    url = db.Column(db.String(const.MAX_URL_LENGTH), unique=True)
    name = db.Column(db.String(const.MAX_NAME_LENGTH))
    description = db.Column(db.String(const.MAX_DESCRIPTION_LENGTH))
    price = db.Column(db.Float)

    reviews = db.relationship("Review", backref="product")


class Order(db.Model):  # type: ignore[name-defined]
    """Order model."""

    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey("user.id"))
    timestamp = db.Column(db.DateTime, server_default=db.func.now())
    total = db.Column(db.Float)

    items = db.relationship("OrderItem", backref="order")


class OrderItem(db.Model):  # type: ignore[name-defined]
    """Order item model."""

    id = db.Column(db.Integer, primary_key=True)
    order_id = db.Column(db.Integer, db.ForeignKey("order.id"))
    product_id = db.Column(db.Integer, db.ForeignKey("product.id"))
    quantity = db.Column(db.Integer)

    product = db.relationship("Product", foreign_keys=[product_id])


class Review(db.Model):  # type: ignore[name-defined]
    """Review model."""

    id = db.Column(db.Integer, primary_key=True)
    text = db.Column(db.String(const.MAX_REVIEW_LENGTH))
    product_id = db.Column(db.Integer, db.ForeignKey("product.id"))


def init_db(app: Flask) -> None:
    """Initialize the database."""

    app.config["SQLALCHEMY_DATABASE_URI"] = os.environ.get("DATABASE_URL")

    if "DATABASE_URL" not in os.environ.keys():
        app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///database.db"

    db.init_app(app)
    with app.app_context():
        db.create_all()

        if Product.query.count() == 0:
            add_products()


def add_products() -> None:
    """Add products to the database."""

    products = [
        {
            "url": "cupcake",
            "name": "Cupcake",
            "description": "A delicious cupcake",
            "price": 1.00,
        },
        {
            "url": "donut",
            "name": "Donut",
            "description": "A delicious donut",
            "price": 0.75,
        },
        {
            "url": "eclair",
            "name": "Eclair",
            "description": "A delicious eclair",
            "price": 2.00,
        },
    ]

    for product in products:
        db.session.add(Product(**product))

    db.session.commit()
