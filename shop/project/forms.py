"""Shop application forms."""

from flask_wtf import FlaskForm
from wtforms import EmailField, IntegerField, PasswordField, StringField, SubmitField
from wtforms.validators import InputRequired, Length, NumberRange

from . import const


class SignupForm(FlaskForm):
    """User signup form."""

    first_name = StringField(
        "First Name", validators=[InputRequired(), Length(const.MIN_FIRST_NAME_LENGTH, const.MAX_FIRST_NAME_LENGTH)]
    )
    last_name = StringField(
        "Last Name", validators=[InputRequired(), Length(const.MIN_LAST_NAME_LENGTH, const.MAX_LAST_NAME_LENGTH)]
    )
    email = EmailField("Email", validators=[InputRequired(), Length(const.MIN_EMAIL_LENGTH, const.MAX_EMAIL_LENGTH)])
    password = PasswordField(
        "Password", validators=[InputRequired(), Length(const.MIN_PASSWORD_LENGTH, const.MAX_PASSWORD_LENGTH)]
    )
    submit = SubmitField("Sign up")


class LoginForm(FlaskForm):
    """User login form."""

    email = EmailField("Email", validators=[InputRequired(), Length(const.MIN_EMAIL_LENGTH, const.MAX_EMAIL_LENGTH)])
    password = PasswordField(
        "Password", validators=[InputRequired(), Length(const.MIN_PASSWORD_LENGTH, const.MAX_PASSWORD_LENGTH)]
    )
    submit = SubmitField("Log in")


class DetailsForm(FlaskForm):
    """Personal details form."""

    email = EmailField("Email", validators=[InputRequired(), Length(const.MIN_EMAIL_LENGTH, const.MAX_EMAIL_LENGTH)])
    first_name = StringField(
        "First Name", validators=[InputRequired(), Length(const.MIN_FIRST_NAME_LENGTH, const.MAX_FIRST_NAME_LENGTH)]
    )
    last_name = StringField(
        "Last Name", validators=[InputRequired(), Length(const.MIN_LAST_NAME_LENGTH, const.MAX_LAST_NAME_LENGTH)]
    )
    address = StringField("Address", validators=[InputRequired(), Length(1, const.MAX_ADDRESS_LENGTH)])
    city = StringField("City", validators=[InputRequired(), Length(1, const.MAX_CITY_LENGTH)])
    submit = SubmitField("Save")


class ReviewForm(FlaskForm):
    """Review form."""

    text = StringField("Review", validators=[InputRequired(), Length(1, const.MAX_REVIEW_LENGTH)])
    submit = SubmitField("Submit")


class CartForm(FlaskForm):
    """Cart form."""

    product_url = StringField("Product", validators=[InputRequired(), Length(1, const.MAX_URL_LENGTH)])
    quantity = IntegerField("Quantity", validators=[NumberRange(min=1, max=100)])
    submit = SubmitField("Add to cart")
