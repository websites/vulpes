### Eshop
Eshop umožňující přihlášení, vytvoření účtu asociovaného s mailem, online nakupování.
Uživatel si k účtu může uložit osobní údaje pro urychlení objednávání.
Aplikace je náchylná na XSS, stored scripty může útočník uložit do reviews k produktům.
