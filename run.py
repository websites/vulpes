#!/usr/bin/env python3
"""Run script for the project."""

import os
import sys

import gevent
from gevent import monkey
from gevent.pywsgi import WSGIServer

monkey.patch_all()

from attacker.project import create_app as create_attacker_app  # noqa: E402
from chat.project import create_app as create_chat_app  # noqa: E402
from index.project import create_app as create_index_app  # noqa: E402
from mail.project import create_app as create_mail_app  # noqa: E402
from shop.project import create_app as create_shop_app  # noqa: E402

APPS = {
    "index": (0, create_index_app),
    "mail": (1, create_mail_app),
    "chat": (2, create_chat_app),
    "shop": (3, create_shop_app),
    "attacker": (4, create_attacker_app),
}


def run_server(domain, port, apps):
    """Run the selected applications."""

    servers = []

    print("Running")
    for app in apps:
        port_offset, constructor = APPS[app]
        url = f"http://{domain}:{port + port_offset}"

        print(f"    {app} on {url}")
        os.environ[f"{app.upper()}_URL"] = url
        servers.append(WSGIServer((domain, port + port_offset), constructor()))

    gevent.joinall([gevent.spawn(server.serve_forever) for server in servers])


def print_help():
    """Print help to the console."""

    print(
        """Usage: run.py [APPS]
Run Vulpes applications on localhost:5000 - :5004.
APPS: index, mail, chat, shop, attacker

  Apps can be specified in any order and with a trailing slash.
  If no apps are specified, all will be run.

  -h, --help: Print this help message and exit."""
    )


if __name__ == "__main__":

    apps = sys.argv[1:]
    apps = [app.rstrip("/") for app in apps]

    if not apps:
        apps = ["index", "mail", "chat", "shop", "attacker"]

    elif apps[0] in ["-h", "--help"]:
        print_help()
        sys.exit(0)

    for app in apps:
        if app not in APPS:
            print_help()
            sys.exit(1)

    run_server("localhost", 5000, apps)
